import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="corcua",
    version="0.0.1",
    author="Jaco Erithacus",
    author_email="jaco@mail.de",
    description="Library to load audio datasets and export them to different framework formats",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "audioread",
        "chardet",
        "pydub",
        "pandas",
        "progressist",
        "soundfile",
        "tqdm",
        "vctube@git+https://github.com/DanBmh/aud-crawler.git@some_improvements",
    ],
    python_requires=">=3.6",
    include_package_data=True,
    package_data={"": ["extras/*", "extras/invalid-files/*"]},
)
