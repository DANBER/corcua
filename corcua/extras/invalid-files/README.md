# Invalid files

Some datasets contain invalid files. Those are collected here and will be excluded by the readers. \
A part of the files is taken from [audiomate](https://github.com/ynop/audiomate/tree/master/audiomate/corpus/io/data).
