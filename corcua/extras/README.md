# Collection of various tools

Mostly used for special dataset preprocessing steps.

### Split mTEDx audio files

The files are very long, so we have to split them into chunks first, using the given transcription timestamps.
This will take some hours.

```bash
python3 corcua/extras/mtedx-splitting.py --path_source ../data_original/de/mTEDx/de-de/data/ --path_target ../data_original/de/mTEDx2/ --lang de
```
