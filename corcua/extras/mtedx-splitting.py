import argparse
import json
import os
from typing import List

import tqdm
import yaml
from pydub import AudioSegment

# ==================================================================================================


def read_data(path: str, part: str, lang: str) -> List[dict]:

    path_yaml = os.path.join(path, part, "txt/{}.yaml".format(part))
    with open(path_yaml, "r", encoding="utf-8") as file:
        sections: List[dict] = yaml.load(file, Loader=yaml.SafeLoader)

    path_text = os.path.join(path, part, "txt/{}.{}".format(part, lang))
    with open(path_text, "r", encoding="utf-8") as file:
        texts = file.readlines()

    # Merge sections and texts
    for i, sec in enumerate(sections):
        sec["text"] = texts[i].strip()

    # Merge sections and texts
    for sec in sections:
        sec["wav"] = "{}/wav/".format(part) + sec["wav"]

    return sections


# ==================================================================================================


def split_files(path_source: str, path_target: str, dataset: List[dict]) -> List[dict]:
    splitted_ds = []
    for entry in tqdm.tqdm(dataset):

        wav_source = os.path.join(path_source, entry["wav"])
        wav_source = wav_source.replace(".wav", ".flac")

        wav_target = os.path.join(path_target, entry["wav"])
        wav_target = wav_target.replace(".wav", "_{}.wav").format(int(entry["offset"]))
        apath = wav_target.replace(path_target, "")

        # Add some extra time, last word often was not fully complete
        duration = entry["duration"] + 0.1
        cut_wav_part(wav_source, wav_target, entry["offset"], duration)

        spentr = {
            "filepath": apath,
            "speaker": entry["speaker_id"],
            "text": entry["text"],
        }
        splitted_ds.append(spentr)

    return splitted_ds


# ==================================================================================================


def cut_wav_part(
    path_source: str, path_target: str, offset: float, duration: float
) -> None:
    start = int(offset * 1000)
    stop = start + int(duration * 1000)
    newAudio = AudioSegment.from_file(path_source)
    newAudio = newAudio[start:stop]

    if not os.path.isdir(os.path.dirname(path_target)):
        os.makedirs(os.path.dirname(path_target))

    newAudio.export(path_target, format="wav")


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="Split mTEDx audio files")
    parser.add_argument(
        "--path_source",
        required=True,
        help="Path to data directory of dataset",
    )
    parser.add_argument(
        "--path_target",
        required=True,
        help="Path to directory to save splitted files",
    )
    parser.add_argument(
        "--lang",
        required=True,
        help="Language of dataset",
    )
    args = parser.parse_args()

    sec_test = read_data(args.path_source, "test", args.lang)
    sec_train = read_data(args.path_source, "train", args.lang)
    sec_valid = read_data(args.path_source, "valid", args.lang)

    dataset = sec_test
    dataset.extend(sec_train)
    dataset.extend(sec_valid)

    new_ds = split_files(args.path_source, args.path_target, dataset)

    json_out = os.path.join(args.path_target, "all.json")
    with open(json_out, "w+", encoding="utf-8") as file:
        json.dump(new_ds, file, indent=2)


# ==================================================================================================

if __name__ == "__main__":
    main()
