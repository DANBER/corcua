from .abstract_downloader import AbstractDownloader

# ==================================================================================================


class Downloader(AbstractDownloader):
    def __init__(self) -> None:
        self.link = "https://www.openslr.org/resources/95/thorsten-de_v02.tgz"

    # ==============================================================================================

    def download_dataset(self, path: str, overwrite: bool, args: dict) -> None:
        super().download_dataset(path, overwrite, args)

        AbstractDownloader.download_and_extract_targz(self.link, path)
