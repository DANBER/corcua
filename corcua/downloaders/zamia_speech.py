import re
from urllib import request

import tqdm

from .abstract_downloader import AbstractDownloader

# ==================================================================================================


class Downloader(AbstractDownloader):
    def __init__(self) -> None:
        self.link = "https://goofy.zamia.org/zamia-speech/corpora/zamia_de/"

    # ==============================================================================================

    def download_dataset(self, path: str, overwrite: bool, args: dict) -> None:
        super().download_dataset(path, overwrite, args)

        # Extract file names from the directory path
        with request.urlopen(self.link) as url:
            page = url.read().decode("utf-8")
        pattern = re.compile(r'href="(.*\.tgz)"')
        files = pattern.findall(page)

        print("Downloading {} files ...".format(len(files)))
        for file in tqdm.tqdm(files):
            flink = self.link + file
            AbstractDownloader.download_and_extract_targz(
                flink, target_path=path, show_progress=False
            )
