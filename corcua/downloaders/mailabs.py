from .abstract_downloader import AbstractDownloader

# ==================================================================================================


class Downloader(AbstractDownloader):
    def __init__(self) -> None:
        self.base_link = "https://www.caito.de/data/Training/stt_tts/{}.tgz"
        self.languages = [
            "de_DE",
            "en_US",
            "en_UK",
            "es_ES",
            "fr_FR",
            "it_IT",
            "pl_PL",
            "ru_RU",
            "uk_UK",
        ]

    # ==============================================================================================

    def download_dataset(self, path: str, overwrite: bool, args: dict) -> None:
        if "language" not in args:
            raise AttributeError("Some arguments are missing")

        super().download_dataset(path, overwrite, args)

        if not args["language"] in self.languages:
            msg = "There is no mailabs URL present for language {}!"
            raise ValueError(msg.format(args["language"]))

        link = self.base_link.format(args["language"])
        AbstractDownloader.download_and_extract_targz(link, path)
