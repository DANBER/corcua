from .abstract_downloader import AbstractDownloader

# ==================================================================================================


class Downloader(AbstractDownloader):
    def __init__(self) -> None:
        self.base_link = "https://www.openslr.org/resources/100/mtedx_{}.tgz"
        self.languages = [
            "ar-ar",
            "de-de",
            "el-el",
            "es-es",
            "fr-fr",
            "it-it",
            "pt-pt",
            "ru-ru",
        ]

    # ==============================================================================================

    def download_dataset(self, path: str, overwrite: bool, args: dict) -> None:
        if "language" not in args:
            raise AttributeError("Some arguments are missing")

        super().download_dataset(path, overwrite, args)

        if not args["language"] in self.languages:
            msg = "There is no mTEDx URL present for language {}!"
            raise ValueError(msg.format(args["language"]))

        link = self.base_link.format(args["language"])
        AbstractDownloader.download_and_extract_targz(link, path)
