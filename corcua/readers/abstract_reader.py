from abc import ABCMeta, abstractmethod
from typing import List

# ==================================================================================================


class AbstractReader:
    __metaclass__ = ABCMeta

    # ==============================================================================================

    @abstractmethod
    def load_dataset(self, args: dict) -> List[dict]:
        return [{}]
