import json
import os
from typing import List

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:

        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        fpath = os.path.join(args["path"], "all.json")
        with open(fpath, "r", encoding="utf-8") as file:
            dataset: List[dict] = json.load(file)

        for entry in dataset:
            ap = os.path.join(args["path"], entry["filepath"])
            entry["filepath"] = ap

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
