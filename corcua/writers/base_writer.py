import multiprocessing as mp
import os
from functools import partial
from typing import List

import pandas as pd
import tqdm

from ..utils import PathSaveString, get_duration, maybe_add_duration, seconds_to_hours
from .abstract_writer import AbstractWriter

# ==================================================================================================


class Writer(AbstractWriter):
    def __init__(self) -> None:
        self.pSS = PathSaveString()

    # ==============================================================================================

    def save_dataset(
        self, dataset: List[dict], path: str, sample_rate: int, overwrite: bool
    ) -> None:

        super().save_dataset(dataset, path, sample_rate, overwrite)

        path_audios = os.path.join(path, "audios")
        os.makedirs(path_audios, exist_ok=True)

        if overwrite:
            # Generate new audio file names
            audio_mappings = []
            for entry in dataset:
                # Generate new audio file names
                old_path: str = entry["filepath"]
                new_path_rel = "audios/" + self.pSS.encode(old_path) + ".wav"
                new_path_abs = os.path.abspath(os.path.join(path, new_path_rel))
                audio_mappings.append((old_path, new_path_abs))
                entry["filepath"] = new_path_rel

            print("\nSaving audio ...")
            self.convert_audio(audio_mappings, sample_rate=sample_rate)

            print("\nExtend dataset ...")
            pfunc_u = partial(self.update_entry, path=path)
            with mp.Pool(mp.cpu_count()) as p:
                dataset = list(tqdm.tqdm(p.imap(pfunc_u, dataset), total=len(dataset)))

        else:
            # If only the csv files are updated, remove the directory path from the abolute paths
            # which are returned by the base-reader, that the default paths are kept the same
            for entry in dataset:
                abspath = os.path.abspath(path) + "/"
                entry["filepath"] = entry["filepath"].replace(abspath, "")

            # Add duration values if not already existing
            dataset = maybe_add_duration(dataset)

        print("\nWriting annotations ...")
        # Write full dataset
        ds_pd = pd.DataFrame(dataset)
        csv_path = os.path.join(path, "all.csv")
        ds_pd.to_csv(csv_path, index=False, encoding="utf-8", sep="\t")

        # Write partitions
        if "partition" in ds_pd.columns.values.tolist():
            parts = set(ds_pd["partition"])
            for part in parts:
                dsp = ds_pd[ds_pd["partition"] == part]
                csv_path = os.path.join(path, "{}.csv".format(part))
                dsp.to_csv(csv_path, index=False, encoding="utf-8", sep="\t")

        msg = "Total length of the dataset is {} hours"
        duration = ds_pd["duration"].sum()
        print(msg.format(seconds_to_hours(duration)))

    # ==============================================================================================

    @staticmethod
    def update_entry(entry: dict, path: str) -> dict:
        # Replace our delimiter in the texts
        entry["text"] = entry["text"].replace("\t", "  ")

        # Add audio duration
        entry["duration"] = get_duration(os.path.join(path, entry["filepath"]))

        return entry
