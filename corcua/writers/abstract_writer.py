import multiprocessing as mp
import os
import shutil
from abc import ABCMeta, abstractmethod
from functools import partial
from io import BytesIO
from typing import List, Tuple

import tqdm
from pydub import AudioSegment

# ==================================================================================================


class AbstractWriter:
    __metaclass__ = ABCMeta

    # ==============================================================================================

    @abstractmethod
    def save_dataset(
        self, dataset: List[dict], path: str, sample_rate: int, overwrite: bool
    ) -> None:

        if len(dataset) == 0:
            raise ValueError("Dataset is empty!")

        if os.path.exists(path):
            if overwrite:
                shutil.rmtree(path, ignore_errors=True)
            else:
                msg = "WARNING: The given directory already exists: {}"
                print(msg.format(path))

        os.makedirs(path, exist_ok=True)

    # ==============================================================================================

    @staticmethod
    def audio_to_mono_wav(paths: Tuple[str, str], sample_rate: int) -> None:
        path_source = paths[0]
        path_target = paths[1]

        if not os.path.exists(path_source):
            msg = "This file does not exist: {}".format(path_source)
            raise FileNotFoundError(msg)

        _, extension = os.path.splitext(os.path.basename(path_source))
        extension = extension.replace(".", "")

        if extension == "opus":
            with open(path_source, "rb") as file:
                opus_audio_bytes = file.read()
            opus_data = BytesIO(opus_audio_bytes)
            audio = AudioSegment.from_file(opus_data, codec="opus")
        else:
            audio = AudioSegment.from_file(path_source, extension)

        audio = audio.set_frame_rate(sample_rate)
        audio = audio.set_channels(1)
        audio.export(path_target, codec="pcm_s16le", format="wav")

    # ==============================================================================================

    @staticmethod
    def convert_audio(audio_mappings: List[Tuple[str, str]], sample_rate: int) -> None:
        """Convert list of audio files with input and output paths from various encodings
        into mono-channel wav format with given sample rate."""

        pfunc = partial(AbstractWriter.audio_to_mono_wav, sample_rate=sample_rate)
        with mp.Pool(mp.cpu_count()) as p:
            list(tqdm.tqdm(p.imap(pfunc, audio_mappings), total=len(audio_mappings)))
