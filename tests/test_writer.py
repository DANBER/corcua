import os
import shutil

from corcua import writers  # pylint: disable=unused-import
from corcua.readers.base_reader import Reader as BaseReader
from corcua.writers.base_writer import Writer as BaseWriter

# ==================================================================================================


def test_writing() -> None:
    # Load our test dataset
    file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
    datapath = file_path + "data/base_dataset/"
    ds = BaseReader().load_dataset({"path": datapath})

    # Add an additional entry to test partition splitting
    ds.append(dict(ds[0]))
    ds[0]["partition"] = "train"
    ds[1]["partition"] = "test"

    # Test writing
    path = "/tmp/coruca/testw/"
    BaseWriter().save_dataset(ds, path=path, sample_rate=16000, overwrite=True)
    assert os.path.exists("/tmp/coruca/testw/all.csv")
    assert os.path.exists("/tmp/coruca/testw/train.csv")
    assert os.path.exists("/tmp/coruca/testw/test.csv")

    # Make sure that we can read it again
    ds = BaseReader().load_dataset({"path": path})
    assert len(ds) == 2

    # Delete created files
    shutil.rmtree("/tmp/coruca/testw/", ignore_errors=True)
