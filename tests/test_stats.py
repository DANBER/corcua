import os

from corcua import stats
from corcua.readers.base_reader import Reader as BaseReader

# ==================================================================================================


def test_get_duration() -> None:
    # Load our test dataset
    file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
    datapath = file_path + "data/base_dataset/"
    ds = BaseReader().load_dataset({"path": datapath})

    ds = stats.get_duration(ds)
    assert ds[0]["duration"] == 4.572
