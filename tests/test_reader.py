import os

from corcua import readers  # pylint: disable=unused-import
from corcua.readers.base_reader import Reader as BaseReader

# ==================================================================================================


def test_reading() -> None:
    file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
    datapath = file_path + "data/base_dataset/"
    ds = BaseReader().load_dataset({"path": datapath})

    assert len(ds) == 1
    assert os.path.exists(ds[0]["filepath"])

    text = "they were subjected to constant surveillance and periodic searches"
    assert ds[0]["text"] == text
